<?php $id="top";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>



<div class="c-header2">
	<div class="c-header__inner">
		<div class="c-header2__flex">
		<div class="c-header2__logo">
			<img src="assets/image/common/com-02.png" alt="">
			<img src="assets/image/common/com-03.png" alt="">
		</div>
		<nav class="c-nav">
			<ul>
			    <li>
			    	<a href="">
				    	<img src="assets/image/common/com-04.png" alt="">
				    </a>
				</li>
			    <li>
			    	<a href="about.php">
				    	<img src="assets/image/common/com-05.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="nogyo.php">
				    	<img src="assets/image/common/com-06.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-07.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-08.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-09.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-10.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-11.png" alt="">
				    </a>
				</li>
			</ul>
		</nav>
		</div>
	</div>
</div>
<div class="l-container">
	<div class="c-slide">
		<div >
			<img src="assets/image/top/top-01.png" alt="">
		</div>
		<div >
			<img src="assets/image/top/top-01.png" alt="">
		</div>
		<div >
			<img src="assets/image/top/top-01.png" alt="">
		</div>
		<div >
			<img src="assets/image/top/top-01.png" alt="">
		</div>
		<div >
			<img src="assets/image/top/top-01.png" alt="">
		</div>
	</div>
	<div class="l-main">
	<section class="p-top1">
		<div class="c-top1">
			<div class="c-top1__ttl1">
				<p>
					NEWS & TOPICS
				</p>
			</div>
			<div class="c-top1__ttl2">
				<img src="assets/image/top/top-02.png" alt="">
				<img src="assets/image/top/top-03.png" alt="">
			</div>
			<div class="c-list1">
				<ul>
				    <li>
				    	<span>
				    		2017.12.20
				    	</span>
				    	<div class="c-btn1">
				    		<img src="assets/image/top/top-04.png" alt="">
				    	</div>
				    	<div class="c-list1__img">
				    		<img src="assets/image/top/top-07.png" alt="">
				    	</div>
				    </li>
				    <li>
				    	<span>
				    		2017.12.20
				    	</span>
				    	<div class="c-btn1 c-btn1--bg2">
				    		<img src="assets/image/top/top-05.png" alt="">
				    	</div>
				    	<div class="c-list1__img">
				    		<img src="assets/image/top/top-08.png" alt="">
				    	</div>
				    </li>
				    <li>
				    	<span>
				    		2017.12.20
				    	</span>
				    	<div class="c-btn1 c-btn1--bg2">
				    		<img src="assets/image/top/top-05.png" alt="">
				    	</div>
				    	<div class="c-list1__img">
				    		<img src="assets/image/top/top-08.png" alt="">
				    	</div>
				    </li>
				    <li>
				    	<span>
				    		2017.12.20
				    	</span>
				    	<div class="c-btn1 c-btn1--bg2">
				    		<img src="assets/image/top/top-05.png" alt="">
				    	</div>
				    	<div class="c-list1__img">
				    		<img src="assets/image/top/top-08.png" alt="">
				    	</div>
				    </li>
				    <li>
				    	<span>
				    		2017.12.20
				    	</span>
				    	<div class="c-btn1 c-btn1--bg3">
				    		<img src="assets/image/top/top-06.png" alt="">
				    	</div>
				    	<div class="c-list1__img">
				    		<img src="assets/image/top/top-09.png" alt="">
				    	</div>
				    </li>
				</ul>
			</div>
		</div>
	</section>
	<section class="p-top2">
		<div class="c-top2">
			<div class="p-top2title">
				<div class="c-title1">
					<p>安心、安全  鹿追ブランド</p>
					<span>Shikaoi Brand</span>
				</div>
			</div>
			
			<div class="c-list2">
				<div class="c-list2__card">
					<div class="c-list2__img1">
						<img src="assets/image/top/shikaoi_brand01.jpg" alt="">
					</div>
					<div class="c-list2__info">
						<h6>肥沃な大地で生産する安全な農作物</h6>
						<p>
							テキストテキストテキストテキストテキストテキストテキスト<br>
							テキストテキストテキストテキストテキスト
						</p>
					</div>
				</div>
				<div class="c-list2__card">
					<div class="c-list2__img1">
						<img src="assets/image/top/shikaoi_brand02.jpg" alt="">
					</div>
					<div class="c-list2__info">
						<h6>北海道を代表する高品質の牛乳</h6>
						<p>
							テキストテキストテキストテキストテキストテキストテキスト<br>
							テキストテキストテキストテキストテキスト
						</p>
					</div>
				</div>
				<div class="c-list2__card">
					<div class="c-list2__img1">
						<img src="assets/image/top/shikaoi_brand03.jpg" alt="">
					</div>
					<div class="c-list2__info">
						<h6>信頼の「鹿追ブランド」牛肉・豚肉</h6>
						<p>
							テキストテキストテキストテキストテキストテキストテキスト<br>
							テキストテキストテキストテキストテキスト
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="c-top2">
			<div class="c-title1">
				<p>青年部・女性部・熟年会</p>
				<span>Shikaoi Community</span>
			</div>
			<div class="c-list2 c-list2--con2">
				<div class="c-list2__card">
					<div class="c-list2__img1">
						<img src="assets/image/top/topj-01.jpg" alt="">
						<div class="c-list2__abs">
							<div class="c-tag">
								<p>
									ダミー写真
								</p>
							</div>
						</div>
					</div>
					<div class="c-list2__info">
						<h6>青年部</h6>
						<p>
							テキストテキストテキストテキストテキストテキストテキスト<br>
							テキストテキストテキストテキストテキスト
						</p>
					</div>
				</div>
				<div class="c-list2__card">
					<div class="c-list2__img1">
						<img src="assets/image/top/topj-02.jpg" alt="">
						<div class="c-list2__abs">
							<div class="c-tag">
								<p>
									ダミー写真
								</p>
							</div>
						</div>
					</div>
					<div class="c-list2__info">
						<h6>女性部</h6>
						<p>
							テキストテキストテキストテキストテキストテキストテキスト<br>
							テキストテキストテキストテキストテキスト
						</p>
					</div>
				</div>
				<div class="c-list2__card">
					<div class="c-list2__img1">
						<img src="assets/image/top/topj-03.jpg" alt="">
						<div class="c-list2__abs">
							<div class="c-tag">
								<p>
									ダミー写真
								</p>
							</div>
						</div>
					</div>
					<div class="c-list2__info">
						<h6>熟年会</h6>
						<p>
							テキストテキストテキストテキストテキストテキストテキスト<br>
							テキストテキストテキストテキストテキスト
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
	<section class="p-top3">
		<div class="c-top3">
			<div class="c-title1 c-title1--sty2">
				<p>フォトギャラリー</p>
				<span>
					Photo Gallery
				</span>
			</div>
			<div class="c-top3__img1">
				<img src="assets/image/top/photogallery_banner01.jpg" alt="">
			</div>
			<div class="c-top3__img1">
				<img src="assets/image/top/photogallery_banner02.jpg" alt="">
			</div>
			<div class="c-top3__img1">
				<img src="assets/image/top/photogallery_banner03.jpg" alt="">
			</div>

		</div>
	</section>
	<section class="p-top4">
		<div class="l-main">
			<div class="c-title1 c-title1--sty2">
				<p>インフォメーション</p>
				<span>Information</span>
			</div>
			<div class="c-top4">
			<div class="c-list3">
				<div class="c-list3__img1">
					<img src="assets/image/top/top-14.png" alt="">
				</div>
				<div class="c-list3__text">
					<p class="c-list3__txt1">Aコープ鹿追店</p>
					<h6>国産野菜統一宣言！ 地産地消をおいしく応援中。</h6>
					<p>
						Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br>
						安心で安全な食品をご提供いたします。
					</p>
				</div>
			</div>
			<div class="c-list3">
				<div class="c-list3__img1">
					<img src="assets/image/top/furusato_banner.jpg" alt="">
				</div>
				<div class="c-list3__text">
					<p class="c-list3__txt1">ふるさと納税返礼品［鹿追町］</p>
					<h6>JA鹿追町のふるさとチョイス。</h6>
					<p>
						安心、安全な鹿追産の牛肉、豚肉、ハンバーグをご提供いたします。<br>
						是非ご利用ください。
					</p>
				</div>
			</div>
			<div class="c-list3">
				<div class="c-list3__img1">
					<img src="assets/image/top/recruit_banner.jpg" alt="">
				</div>
				<div class="c-list3__text">
					<p class="c-list3__txt1">農業求人情報 <span class="c-left">new</span> </p>
					<h6>酪農スタッフ募集のお知らせ。</h6>
					<p>
						酪農スタッフを募集しています。<br>
						広大な十勝平野で私たちと一緒に働きませんか！
					</p>
				</div>
			</div>
			<div class="c-list3 c-list3--af2">
				<div class="c-list3__img1">
					<img src="assets/image/top/recipe_banner.jpg" alt="">
				</div>
				<div class="c-list3__text">
					<p class="c-list3__txt1">とっておきのレシピ<span>new</span></p>
					<h6>野菜の大根おろしあえ vol.89</h6>
					<p>
						旬の野菜を大根おろしであえ特性タレをかけました。<br>
						ごはんのおかずにもビールのおつまににも良く合います。
					</p>
				</div>
			</div>
			<div class="c-list3">
				<div class="c-list3__img1">
					<img src="assets/image/top/jabook_banner.jpg" alt="">
				</div>
				<div class="c-list3__text">
					<p class="c-list3__txt1">JA通信しかおい</p>
					<h6>JA通信11月号を公開しました。</h6>
					<p>
						農業ニュースや各種イベントなど楽しい話題をお届けいたします。<br>
						鹿追町内の皆さまへ毎月無料配布をおこなっています。
					</p>
				</div>
			</div>
			</div>
		</div>
	</section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>

