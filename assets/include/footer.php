<footer>
	<div class="l-main">
		<div class="c-footer">
			<div class="c-footer__left">
				<div class="c-footer__img1">
					<img src="assets/image/top/top-11.png" alt="">
				</div>
				<div class="c-footer__btn">
					<div class="c-btn2">
						<a href="">アクセスマップ</a>
					</div>
					<div class="c-btn2">
						<a href="">お問い合わせ</a>
					</div>
				</div>
			</div>
			<div class="c-footer__right">
				<div class="c-footer__img2">
					<img src="assets/image/top/top-12.png" alt="">
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="/assets/js/functions.js"></script>
</body>
</html>