<?php $id="about";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-header2">
	<div class="c-header__inner">
		<div class="c-header2__flex">
		<div class="c-header2__logo">
			<img src="assets/image/common/com-02.png" alt="">
			<img src="assets/image/common/com-03.png" alt="">
		</div>
		<nav class="c-nav">
			<ul>
			    <li>
			    	<a href="top.php">
				    	<img src="assets/image/common/com-12.png" alt="">
				    </a>
				</li>
			    <li>
			    	<a href="about.php">
				    	<img src="assets/image/common/com-13.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="nogyo.php">
				    	<img src="assets/image/common/com-06.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-07.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-08.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-09.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-10.png" alt="">
				    </a>
				</li>
				<li>
			    	<a href="">
				    	<img src="assets/image/common/com-11.png" alt="">
				    </a>
				</li>
			</ul>
		</nav>
		</div>
	</div>
</div>
<div class="l-container">
	<section class="p-about1">
		<div class="c-about1">
			<div class="c-title2">
				<p>組合長からのご挨拶</p>
				<span>感謝の思いを込めて</span>
			</div>
			<div class="c-about1__info">
				<div class="c-about1__avatar">
					<img src="assets/image/about/ab-01.png" alt="">
				</div>
				<div class="c-about1__text">
					<div class="c-about1__text1">
						<p>
							&emsp;当ＪＡは、農村の民主化と社会的経済的地位の向上を目指して、昭和23年に設立を致しました。<br>
							思えばこの自然環境の美しい鹿追町も我々の先達が困苦欠乏に耐え忍び乍らも、未来に楽土を夢み開拓に汗、血を<br>
							流したのであります。当時は一部の営利商人や金融家の手により流通機構が形成され、中には財産の全てを業者に<br>
							依存せねばならない農家も少なくはなく、 これらの重圧から経営を守る組織として産業組合が設立され、農業協<br>
							同組合へと引き継がれ幾多の試練を乗り越え今日に至りました。
						</p>
					</div>
					<div class="c-about1__text1">
						<p>
							&emsp;今日では想いも及ばない事ではありますが、これらの教訓と歴史を礎に現在のＪＡが生き抜いてこられたことであ<br>
							り、 農家組合員そして地域社会の中で生業させて頂いていることに対し深く感謝の念を申し上げます。<br>
							昨今の社会・経済の環境変化が著しい中、当ＪＡが地域社会のリーダーとして理解され親しまれる様、 また、今<br>
							後も巾広く貢献出来る様に事業を推めてまいりますので、皆様のご利用・ご協力を頂きたいと存ずる次第です。<br>
						</p>
					</div>
					<div class="c-about1__sign">
						<p>
							ＪＡ鹿追町　代表理事組合長　木幡浩喜
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="p-about2">
		<div class="c-title2">
			<p>
				ＪＡ綱領
			</p>
			<span>
				わたしたちＪＡのめざすもの
			</span>
		</div>
		<div class="c-blankpic">
			<div class="c-blankpic__abs">
				<div class="c-tag c-tag--ta2">
					<p>
						社屋外観写真
					</p>
				</div>
			</div>
		</div>
		<div class="c-about2">
			<div class="c-about2__text">
				<p>
					&emsp;わたしたちＪＡの組合員・役職員は、協同組合運動の基本的な定義・価値・原則（自主、自立、参加、民主的運営、公正、連帯等）に基づき行動します。<br>
					&nbsp;そして、地球的視野に立って環境変化を見通し、組織・事業・経営の革新を図ります。さらに、地域・全国・世界の協同組合の仲間と連携し、より民主的で公正な社会の実<br>
					現に努めます。このため、わたしたちは次のことを通じ、農業と地域社会に根ざした組織としての社会的役割を誠実に果たします。<br>
					</p>
			</div>
			<div class="c-about2__text">
				<p>
					わたしたちは、<br>
					一、地域の農業を振興し、わが国の食と緑と水を守ろう。<br>
					一、環境・文化・福祉への貢献を通じて、安心して暮らせる豊かな地域社会を築こう。<br>
					一、ＪＡへの積極的な参加と連帯によって、協同の成果を実現しよう。<br>
					一、自主・自立と民主的運営の基本に立ち、ＪＡを健全に経営し信頼を高めよう。<br>
					一、協同の理念を学び実践を通じて、共に生きがいを追求しよう。<br>
				</p>
			</div>
		</div>
	</section>
	<section class="p-about3">
		<div class="l-main">
			<div class="c-title2">
				<p>
					基本理念
				</p>
				<span>
					当ＪＡは４つの基本理念の下、事業展開を行います。
				</span>
			</div>
			<div class="c-about3">
				<div class="c-about3__text1">
					<p>
						&emsp;JAは、人々が連帯し助け合うことを意味する「相互扶助」の精神のもとに、組合員農家の農業経営と生活を守り、より良い地域社会を築くことを<br>
						目的につくられた協同組合です。JA鹿追町は、次の基本理念の下事業を展開します。
					</p>
				</div>
				<div class="c-about3__text2">
					<div class="c-about3__txt1">
						<span>
								1. 真に農協らしい農協
						</span>
						<p>
							組合員のための事業展開（組合員の経済・生活上必要な事業であっても決して農協のためであってはならない）
						</p>
					</div>
					<div class="c-about3__txt1">
						<span>
								2. 正確な情報を正しく提供（公開）出来る農協
						</span>
						<p>
							農協の経営状況・財務等を正しく公開するとともに、農業情勢および営農上必要とする情報を正しく伝え、組合員が適正な判断が出来る様に<br>
							します
						</p>
					</div>
					<div class="c-about3__txt1">
						<span>
								３. 組合員が結集出来る農協
						</span>
						</p>
						<p>
							組合員が必要とする農協。組合員の意見を積極的に聞くとともに動向を把握し事業展開を行う。理念だけではなく、他に負けない実利伴う購
							買・販売事業と組合員に応える利用事業及び農業支援システムの展開
						</p>
					</div>
					<div class="c-about3__txt1">
						<span>
								4. 地域住民に応える農協
						</span>
						<p>
							農家組合員だけでなく、地域の住民に対し金融・共済事業、給油所、整備工場や生活店舗を含む事業により貢献する農協
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="p-about4">
		<div class="l-main">
			<div class="c-title2">
				<p>
					組織
				</p>
				<span>
					概況
				</span>
			</div>
			<div class="c-about4">
				<div class="c-about4__left">
					<p>
						（設立）昭和23年3月1日
					</p>
					<p>（組合員）正組合員 248名　法人 31名　准組合員 997名
					</p>
					<p>
						（役員）理事 14名　監事 5名<br>
					　　　　代表理事組合長 1名<br>
					　　　　専務理事 1名<br>
					　　　　常勤監事 1名<br>
					　　　　非常勤理事 1名<br>
					　　　　非常勤監事 4名<br>
					　　　　　　　 計 19名

					</p>
					<p>（職員）一般職員 76名（男）・33名（女）　計 109名<br>
					　　　　工場技術員 17名（男）計 17名<br>
					　　　　　　　計 93名（男）・33名（女）
					</p>
				</div>
				<div class="c-about4__right">
					<p>
						（出資金）総口数 272,924口<br>
					　　　　　総額 1,264,620千円<br>
					　　　　　一口金額 5,000円<br>
					　　　　　最高限度 10,000口
					</p>
					<p>
						（運営）総会 − 毎年5月開催（通常）<br>
					　　　　理事会 − 毎月1回（定例）<br>
					　　　　監査 − 4半期毎（定例）
					</p>
					<p>
						（外かく組織）　◇ 酪農事業推進部会 − 16名<br>
						　　　　　　　　◇ 農産事業推進部会 − 16名<br>
						　　　　　　　　◇ 畜産事業推進部会 − 4名<br>
						　　　　　　　　◇ 作業受委託事業推進部会 − 16名<br>
						　　　　　　　　◇ 女性の会 − 36名<br>
						　　　　　　　　◇ JA青年部 − 83名<br>
						　　　　　　　　◇ JA女性部 − 141名<br>
						　　　　　　　　  （フレッシュミズ会員54名・喜楽会員50名）<br>
						　　　　　　　　◇ 熟年会 − 29名<br>
						　　　　　　　　◇ 年金友の会 − 901名<br>
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="p-about5">
		<div class="l-main">
			<div class="c-title2">
				<p>
					組織機構図
				</p>
				<span>
					フローチャート
				</span>
			</div>
			<div class="c-blankpic c-blankpic--heg2">
				<div class="c-blankpic__abs">
					<div class="c-tag c-tag--ta2">
						<p>
							組織機構図画像
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="p-about6">
		<div class="l-main">
			<div class="c-title2 c-title2--fz2">
				<p>
					職員募集のお知らせ
				</p>
				<span>
					Recruitment
				</span>
			</div>
			<div class="c-about6">
				<div class="c-about6__text">
					<p>
						JA鹿追町では職員の募集を行っています。<br>
						2018年4月採用 1〜3名を予定しています。
					</p>
					<span class="c-afspan">
						募集要項はこちら
					</span>
				</div>
				<div class="c-about6__img">
					<div class="c-blankpic c-blankpic--heg3">
						<div class="c-blankpic__abs">
							<div class="c-tag c-tag--ta3">
								<p>
									職場の写真
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>